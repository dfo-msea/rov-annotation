## Prepare ROV GPS tracking information for upload into Biigle

# Load raw ROV tracking file
xydat <- read.csv("<rov-tracking-filename>.txt", header = F)

# Add header names, order may vary depending on format of the data
names(xydat) <- c("Dive","Date","Time","Latitude","N",
                  "Longitude","W","Depth")

# Check
head(xydat)

# Ensure that longitude is negative
xydat$Longitude <- -1 * abs(xydat$Longitude)

# Add datetime column
datetime_str <- paste(xydat$Date, substr(xydat$Time, 1, 8))
xydat$Datetime <- as.POSIXct(datetime_str, tz="GMT",
                                format="%m/%d/%Y %H:%M:%S")

# Format transect name, may vary with different tracking data
xydat$Transect <- sub(".EDT", "", xydat$Dive)
xydat$Transect <- sub(".*_", "", xydat$Transect)

# Convert datetime into text for filename
xydat$dt_text <- sub(" ", "_", as.character(xydat$Datetime))
xydat$dt_text <- gsub(":", "", xydat$dt_text)
xydat$dt_text <- gsub("-", "", xydat$dt_text)

# Create filename
xydat$filename <- paste(xydat$Transect, xydat$dt_text, sep="_")

# Export tracking data by transect to upload into Biigle volumes
for( i in unique(xydat$Transect)){

  # Subset rows by transect
  tmp <- xydat[xydat$Transect == i,]

  # Subset columns
  tmp <- tmp[c("filename","Latitude","Longitude","Datetime","Depth")]

  # Rename columns to match Biigle requirements
  names(tmp) <- c("filename","lat","lng","taken_at","gps_altitude")

  # Add .jpeg extention to filename
  tmp$filename <- paste0(tmp$filename, ".jpeg")

  # Export csv file
  write.csv(tmp, row.names = FALSE, paste0(i,"_Tracking4Biigle.csv"))

}