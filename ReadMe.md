# ROV Annotation

__Main author:__  Jessica Nephin  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis (MSEA)   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: jessica.nephin@gmail.com | tel: 250-363-6564


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
  - [Image Annotation](#image-annotation)
  - [Video Annotation](#video-annotation)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
A collection of R, RShiny, RMarkdown and Python scripts that process, visalize and summarise data from image or video BIIGLE projects and iforms survey logs.

## Summary
The project was created to facilitate the use of Biigle (https://biigle.de/) as a tool for the annotation of ROV imagery and video and the use of iforms (https://msea.iformbuilder.com/) for logging dive, transect, deck and video events at-sea. Biigle is a web-application for collaborative annotation of marine images and videos. The image and video annotation scripts are tools that automate input data preparation (e.g. still images from video, tracking data) and post-processing of annotation results (e.g. re-formatting biigle exports, combining transects, QAQC checks, estimate field of view area and summarising results). The shiny application provides an interactive environment for visualizing BIIGLE video annotation data and performing quality control checks.

## Status
Ongoing-improvements

## Contents
The Biigle inputs and output data processing steps vary depending on whether the annotation project involves still images or video clips. Thus, scripts are organised in sub-directories depending on whether they are used in image or video annotation projects.

### Shiny QC
The shiny-qc directory contains app.R that produces a interactive shiny app for quality control of BIIGLE video annotation data. It requires 2 input files, a BIIGLE video annotation report (zip) and ROV tracking data (csv). The app allows you to visualize BIIGLE data in a number of different ways (in space, by dive, by annotator and by label) and preforms a number of quality control checks. The app also gives the user the option of downloaded the processed, georeferenced dataset.

### Image Annotation
The image-annotation directory contains a Python tool (ImageCaptureTool.py) for capturing images at a specified frequency from videos. The directory also contains a number of R scripts (1. through 7.) that prepares CSV image and CSV annotation exports for merging, checks for missing or duplicate laser point annotations, merges CSV image , CSV annotation and Area reports, performs data quality checks on the combined dataset and estimate polygon and field of view areas.

### Video Annotation
The video-annotation directory contains as shell script with instructions for using the command line tool ffmpeg to clip, convert and rename video files. The directory also contains R scripts (1.create-dataset.R and 2.merge-tracking.R) which processes Biigle annotation exports, performing qaqc checks, expanding the time segments annotations (e.g. substrate from one time point to another) into individual records and georeferences annotations by merging the ROV tracking data. The directory also contains RMarkdown scripts (3.qaqc-report.Rmd and 4.summary-report.Rmd) that creates html reports containing tables and figures showing potential annotation errors and summarises of annotation results (e.g. the amount of video annotation completed, how the annotations varied among transects and annotators, and how the ROV tracking info varied by transect).

### Survey Logs
MSEA has created custom forms using iforms builder to log events during ROV surveys. The R script downloads and processes records from trip, dive, transect, deck and video logs. The RMarkdown script creates a html summary report that provides a survey overview, survey map and transect summaries.

## Methods
Methods used for processing Biigle annotation results varied between image and video projects. Generally, R subsetting, reshaping and filtering functionally was used to perform qaqc checks, reformatting, and merging of image or frame level annotations with species level annotations. Methods for area and field of view calculations for both image and video annotation projects were based on the distance between laser point annotations (using the known distance in cm of the laser points to calculate the area of the polygon or frame).  

## Requirements
Input data needed are either videos used for clipping or extracting images and post-annotation Biigle exports. For image projects, csv image and csv annotation reports are required as well as xlsx area reports if polygon annotations were used. For video projects, csv video annotation reports are required.

Video clipping and image extraction requires the ffmpeg command line tool to capture images, download at https://ffmpeg.org/. R scripts require several additional packages, see each sub project for details.

## Caveats
The image annotation R scripts were created using exports from Pac2017-030 survey and video annotation R scripts were created using exports from Pac2019-015 survey and may need to be adapted for other survey data and Biigle annotation protocols.

## Uncertainty
There is uncertainty in the field of view estimates calculated from laser point annotations. More accurate estimates would take into account additional ROV attributes such as altitude, pitch, roll, zoom and range to target.

## Acknowledgements
Jonathan Martin, Sharon Jeffery, Rob Skelly
